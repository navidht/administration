This is the list of everyone currently subscribed to the `CSCI6900-S15@listserv.cc.uga.edu` mailing list. If your name does not appear, or your name / email is incorrect, please add it or change it!

Name | Email
---- | -----
Shannon Quinn | squinn@cs.uga.edu
Yang Bai | yangbai@UGA.EDU
Roi Ceren | ksamim@UGA.EDU
Muthukumaran Chandrasekaran | mkran@UGA.EDU
Alekhya Chennupati | ac59478@UGA.EDU
Michael Church | mchurc90@UGA.EDU
Seyed Tonekaboni | navidht@UGA.EDU
Thomas Horton | thorton@UGA.EDU
Pan Huang | hp240955@UGA.EDU
Ankita Joshi | apj32468@uga.edu
Bita Kazemi | bitak@UGA.EDU
Zhaochong Liu | liu123@UGA.EDU
Manish Ranjan | mr40613@UGA.EDU
William Richardson | wdr525@uga.edu
Victor Ruberti | anthozoa@UGA.EDU
BahaaEddin AlAila | bma09868@UGA.EDU
Yue Yin | yinyue@uga.edu