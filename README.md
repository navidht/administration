# CSCI 6900 Administration

In this repository you'll find information pertaining the course itself. You can edit this repository, but since all changes are recorded, please keep your edits within the context of the current assignments.

Over the course of the semester, I'll add text files to this repository that I need feedback on from everyone.

## Current contents
 - `MAILING_LIST`: This contains the names and UGA IDs of everyone in the course. If your name and email don't show up, please add them!

 - `STUDENT_LECTURES`: This functions as the sign-up sheet to give student lectures. It is required that everyone give 1 lecture over the semester. For more information, check out the course website.

